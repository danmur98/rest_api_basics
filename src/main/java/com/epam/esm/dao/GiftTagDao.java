package com.epam.esm.dao;

public interface GiftTagDao {

        void insertGiftTag(int giftId, int tagId);
        boolean isGiftTagExists(int giftId, int tagId);

}
