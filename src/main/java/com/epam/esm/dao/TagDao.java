package com.epam.esm.dao;


import com.epam.esm.model.Tag;
import java.util.List;
public interface TagDao {

        void insert(Tag tag);
        void delete(int id);
        Tag findTagById(int id);
        List<Tag> findAll();


}
