package com.epam.esm.controller;



import java.io.IOException;
import java.math.BigDecimal;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

import com.epam.esm.dao.GiftCertificateDao;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import com.epam.esm.dao.TagDao;

@Controller
public class ApiController {

	@Autowired

	private final TagDao tagDao;

	@Autowired
	private final GiftCertificateDao giftDao;





	public ApiController (TagDao tagDao, GiftCertificateDao giftDao) {
		this.tagDao = tagDao;
		this.giftDao = giftDao;


	}

	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) throws IOException{
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/gift",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GiftCertificate getGift(HttpServletResponse response) throws IOException{
		GiftCertificate gift = new GiftCertificate();
		gift.setId(1);
		gift.setName("My gift");
		gift.setPrice(new BigDecimal(12.34));
		gift.setDuration(0);
		Date date = new Date();
		gift.setCreateDate(date);
		gift.setLastUpdateDate(date);
		return gift;
	}

	@RequestMapping(value="/gift",
			method=RequestMethod.POST)
	public @ResponseBody GiftCertificate postGift(@RequestBody GiftCertificate gift) throws IOException{
		giftDao.insert(gift);
		return gift;
	}

	@RequestMapping(value="/tag",
			method=RequestMethod.POST)
	public @ResponseBody Tag postGift(@RequestBody Tag tag) throws IOException {
		tagDao.insert(tag);
		return tag;

	}


	//Eliminar un tag:

	@RequestMapping(value = "/tag/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteTag(@PathVariable("id") int id) {
		tagDao.delete(id);
		return ResponseEntity.ok("tag deleted successfully");
	}

	//Eliminar un certificado de regalo:

	@RequestMapping(value = "/gift/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteGift(@PathVariable("id") int id) {
		giftDao.delete(id);
		return ResponseEntity.ok("Gift deleted successfully");
	}

	//Obtener todos los certificados de regalo

	@RequestMapping(value = "/gift", method = RequestMethod.GET)
	public @ResponseBody List<GiftCertificate> getAllGifts() {
		return giftDao.findAll();
	}


	//Obtener un certificado de regalo por su ID:

	@RequestMapping(value = "/gift/{id}", method = RequestMethod.GET)
	public @ResponseBody GiftCertificate getGiftById(@PathVariable("id") int id) throws IOException {
		return giftDao.findById(id);
	}
	//Actualizar un certificado de regalo:

	@RequestMapping(value = "/gift/{id}", method = RequestMethod.PUT)
	public @ResponseBody void updateGift(@PathVariable("id") int id, @RequestBody GiftCertificate gift)throws IOException {
		gift.setId(id);
		giftDao.update(gift);
	}


	//Obtener todos los tags:

	@RequestMapping(value = "/tag", method = RequestMethod.GET)
	public @ResponseBody List<Tag> getAllTags() throws IOException{
		return tagDao.findAll();
	}



	//Obtener un tag por su ID:


	@RequestMapping(value = "/tag/{id}", method = RequestMethod.GET)
	public @ResponseBody Tag getTagById(@PathVariable("id") int id) throws IOException {
		return tagDao.findTagById(id);
	}


}