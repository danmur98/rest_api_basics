package com.epam.esm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class SQL_config {

    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/prueba";
    private static final String DATABASE_USERNAME = "root";
    private static final String DATABASE_PASSWORD = "12345678";

    @Bean
    public static void createTables() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource(DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.execute("DROP TABLE IF EXISTS gift_tag;");
        jdbcTemplate.execute("DROP TABLE IF EXISTS tag;");
        jdbcTemplate.execute("DROP TABLE IF EXISTS gift_certificate;");

        jdbcTemplate.execute("CREATE TABLE gift_certificate (gift_id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255), "
                + "price NUMERIC(12, 2), duration int, create_date TIMESTAMP , last_update_date TIMESTAMP );");

        jdbcTemplate.execute("CREATE TABLE tag (tag_id int PRIMARY KEY, name VARCHAR(255));");

        jdbcTemplate.execute("CREATE TABLE gift_tag (gift_id int, tag_id int, "
                + "FOREIGN KEY (gift_id) REFERENCES gift_certificate (gift_id) ON DELETE CASCADE, "
                + "FOREIGN KEY (tag_id) REFERENCES tag (tag_id) ON DELETE CASCADE, "
                + "PRIMARY KEY (gift_id, tag_id));");
    }

}
