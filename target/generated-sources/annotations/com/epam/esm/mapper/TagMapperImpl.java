package com.epam.esm.mapper;

import com.epam.esm.dto.TagDTO;
import com.epam.esm.model.Tag;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-10T12:34:59-0500",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 1.8.0_332 (Temurin)"
)
public class TagMapperImpl implements TagMapper {

    @Override
    public Tag tagDtoToModel(TagDTO tag) {
        if ( tag == null ) {
            return null;
        }

        Tag tag1 = new Tag();

        return tag1;
    }

    @Override
    public TagDTO tagModelToDto(Tag tag) {
        if ( tag == null ) {
            return null;
        }

        TagDTO tagDTO = new TagDTO();

        return tagDTO;
    }

    @Override
    public List<TagDTO> tagModelToDtoList(List<Tag> tag) {
        if ( tag == null ) {
            return null;
        }

        List<TagDTO> list = new ArrayList<TagDTO>( tag.size() );
        for ( Tag tag1 : tag ) {
            list.add( tagModelToDto( tag1 ) );
        }

        return list;
    }
}
