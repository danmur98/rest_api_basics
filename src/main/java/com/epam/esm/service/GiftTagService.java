package com.epam.esm.service;



import com.epam.esm.dao.GiftCertificateDao;
import com.epam.esm.dao.GiftTagDao;
import com.epam.esm.dao.TagDao;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.springframework.stereotype.Service;


@Service
public class GiftTagService {
    private GiftCertificateDao giftDao;
    private TagDao tagDao;
    private GiftTagDao giftTagDao;

    // Constructor y métodos setter para inyectar las dependencias



    public void setGiftDao(GiftCertificateDao giftDao) {
        this.giftDao = giftDao;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }

    public void setGiftTagDao(GiftTagDao giftTagDao) {
        this.giftTagDao = giftTagDao;
    }
}