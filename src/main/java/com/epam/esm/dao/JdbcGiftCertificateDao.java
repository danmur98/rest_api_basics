package com.epam.esm.dao;


import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcGiftCertificateDao implements GiftCertificateDao {
    private static final String INSERT_SQL = "INSERT INTO gift_certificate ( name, description, price, duration, create_date, last_update_date) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_SQL = "UPDATE gift_certificate SET name=?, description=?, price=?, duration=?, create_date=?, last_update_date=? WHERE gift_id=?";
    private static final String DELETE_SQL = "DELETE FROM gift_certificate WHERE gift_id=?";
    private static final String SELECT_ONE_SQL = "SELECT * FROM gift_certificate WHERE gift_id=?";
    private static final String SELECT_ALL_SQL = "SELECT * FROM gift_certificate";

    private final TagDao tagDao;
    private static final String SELECT_TAGS_BY_GIFT_ID_SQL = "SELECT tag_id FROM gift_tag WHERE gift_id = ?";

    private final DataSource dataSource;

    public JdbcGiftCertificateDao(DataSource dataSource) {
        this.dataSource = dataSource;
        this.tagDao = new JdbcTagDao(dataSource);
    }

    @Override
    public void insert(GiftCertificate giftCertificate) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL)) {
            prepareStatement(ps, giftCertificate);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(GiftCertificate giftCertificate) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {
            prepareStatement(ps, giftCertificate);
            ps.setInt(7, giftCertificate.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public GiftCertificate findById(int id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setInt(1, id);
            GiftCertificate giftCertificate = null;
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next())
                    giftCertificate = toGiftCertificate(rs);
            }
            return giftCertificate;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<GiftCertificate> findAll() {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            List<GiftCertificate> giftCertificates = new ArrayList<>();
            while (rs.next()) {
                giftCertificates.add(toGiftCertificate(rs));
            }
            return giftCertificates;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    private GiftCertificate toGiftCertificate(ResultSet rs) throws SQLException {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setId(rs.getInt("gift_id"));
        giftCertificate.setName(rs.getString("name"));
        giftCertificate.setDescription(rs.getString("description"));
        giftCertificate.setPrice(rs.getBigDecimal("price"));
        giftCertificate.setDuration(rs.getInt("duration"));
        giftCertificate.setCreateDate(rs.getDate("create_date"));
        giftCertificate.setLastUpdateDate(rs.getDate("last_update_date"));
        return giftCertificate;
    }

    private void prepareStatement(PreparedStatement ps, GiftCertificate giftCertificate) throws SQLException {
        ps.setString(1, giftCertificate.getName());
        ps.setString(2, giftCertificate.getDescription());
        ps.setBigDecimal(3, giftCertificate.getPrice());
        ps.setInt(4, giftCertificate.getDuration());
        ps.setTimestamp(5, new java.sql.Timestamp(giftCertificate.getCreateDate().getTime()));
        ps.setTimestamp(6, new java.sql.Timestamp(giftCertificate.getLastUpdateDate().getTime()));
    }
}