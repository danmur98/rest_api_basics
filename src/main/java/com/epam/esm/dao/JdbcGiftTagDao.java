package com.epam.esm.dao;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.activation.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcGiftTagDao {

    private static final String INSERT_GIFT_TAG_SQL = "INSERT INTO gift_tag (gift_id, tag_id) VALUES (?, ?)";
    private static final String CHECK_GIFT_TAG_SQL = "SELECT COUNT(*) FROM gift_tag WHERE gift_id = ? AND tag_id = ?";

    private final DriverManagerDataSource dataSource;

    public JdbcGiftTagDao(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }


    public void insertGiftTag(int giftId, int tagId) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_GIFT_TAG_SQL)) {
            ps.setInt(1, giftId);
            ps.setInt(2, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public boolean isGiftTagExists(int giftId, int tagId) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(CHECK_GIFT_TAG_SQL)) {
            ps.setInt(1, giftId);
            ps.setInt(2, tagId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    int count = rs.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }
}
